---
permalink: /DUN/DUNOnline
layout: page
title: Online DUN
---



----

### 📆 Semana de campaña: Año 2: 1/52 Invierno       
### 💰 Tesorería: 75 monedas     
### ↪️ Partidas disponibles:   
- M10 Cazar a la gran Criatura  
- M21 Ladrones de caballos (nueva mini-campaña)  
- Evento épico "Ayuda"

### 🧙‍♀️ [Objetos mágicos en torres de hechicería](#objetosmagicos-id)
  
----

## Pertsonaiak

| Varyana Piruja    |      | **PV: 30** (24+6)            |      |
| ---------- | :--: | ------------ | :--: |
| Movimiento |   6   | Agilidad     |   4   |
| Ataque     |   2   | Inteligencia |   5   |
| Fuerza     |   3   | Mana         |   17 (15+2)   |
| Disparo    |   4   | Valentia     |   4   |
| Armadura   |   3   | Vitalidad    |   5   |
| Suerte     |   5   | Experiencia  |  0   |
| Destreza   |   0   | Percepción   |  1    |

- *Elfa*-*Bruja*
- **Armas**: Daga y bastón
- **Armaduras**:
- **Equipo**: Poción de maná, **_[Anillo Demoníaco (6)](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Equipo/ObjetosMagicos/anillodemoniaco.png?raw=true)_**, Componentes para magia.
- **Habilidades** (3/10): *Herbología, [Sentidos Agudizados](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Habilidades/sentidosagudizados.png)*, [Habilidad Mental](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/habilidadmental.png?raw=true), Alquimista, Cruel
- **Xp en Pozo de Lucha**: 0/3 (1.Año 2/3)
- **Partidas jugadas:** 14/16
- **Hechizos**: 
- [Brujería (6/6)](https://github.com/IzaroBlog/IzaroBlog.github.io/tree/main/_images/DUN/Hechizos/Brujeria)  
	🌚 [Aire Venenoso](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Brujeria/airevenenoso.png?raw=true)  
	🌚 [Alas negras](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Brujeria/alasnegras.png?raw=true)  
	🌚 [Asfixia](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Brujeria/asfixia.png?raw=true)  
	🌚 [Flecha Oscura](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Brujeria/flechaoscura.png?raw=true)  
	🌚 [Manipular mente](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Brujeria/manipularmente.png?raw=true)  
	🌚🌚 [Llamar a las alimañas](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Hechizos/Brujeria/llamaralasalimanas.png)
- Fuego (2/6)  
	🔥 [Ataques Flamígeros](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Saber%20del%20Fuego/ataquesflamigeros.png?raw=true)  
	🔥 [Bola de Fuego](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Saber%20del%20Fuego/boladefuego.png?raw=true)  


---  



| Chester Cheetos    |      |     **PV: 20**         |      |
| ---------- | :--: | ------------ | :--: |
| Movimiento |  6    | Agilidad     |   4   |
| Ataque     |  4    | Inteligencia |   4   |
| Fuerza     |  3    | Mana         |   -   |
| Disparo    |  5    | Valentia     |   3   |
| Armadura   |  3    | Vitalidad    |   5   |
| Suerte     |  **5**    | Experiencia  |   3  |
| Destreza   |  0    | Percepción   |  **2**   |

- *Félido*-*Explorador*
- **Armas**: Arcabuz, [Garras](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Equipo/garras.png), Cimitarra, 
- **Armaduras**: Armadura de Cuero, Escudo
- **Equipo**: Ganzúas, Poción de curación.  
- **Habilidades** (4/8): *Vista de Águila, [Sentidos Agudizados](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Habilidades/sentidosagudizados.png)*, Cazador, [Disparo Certero](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/disparocertero.png?raw=true), [Recarga rápida](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/recargarapida.png?raw=true), [Emboscar](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/emboscar.jpg?raw=true).
- **Xp en Pozo de Lucha**: 0/3 (1.Año 2/3)
- **Partidas jugadas:** 15/16


---


| [Intxaur Waldosson](#Intxaur-id)     |      |   **PV: 18**           |      |
| ---------- | :--: | ------------ | :--: |
| Movimiento |   4   | Agilidad     |  3    |
| Ataque     |   5   | Inteligencia |  4    |
| Fuerza     |   **5**   | Mana         |  -    |
| Disparo    |   3   | Valentia     |  4    |
| Armadura   |   3   | Vitalidad    |  6    |
| Suerte     |   5   | Experiencia  |  4    |
| Destreza   |   1   | Percepción   |  0    |

- *[Enano](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/RazasyProfesiones/enano.png)*- *Guerrero*
- **Armas**: Hacha, Espada Ancha. 
- **Armaduras**: Cota de Malla y Escudo enano.
- **Equipo**: Pócima curativa, Pócima de la Destreza.
- **Habilidades** (4/8): *Odio a Orcos y Goblins, [Manos hábiles](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Habilidades/manoshabiles.png), [Resistente](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/Habilidades/resistente.png), Curtido*, Sobrellevar Armadura, [Escolta](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/escolta.png?raw=true), Forzuda, Maestra en Combate.
- **Xp en Pozo de Lucha**: 0/3 (1.Año 0/3)
- **Partidas jugadas:** 15/15

---

| Bergoglio    |      |      **PV: 14**        |      |
| ---------- | :--: | ------------ | :--: |
| Movimiento |  5    | Agilidad     |   3   |
| Ataque     |  5    | Inteligencia |   4   |
| Fuerza     |  4    | Mana         |   4   |
| Disparo    |  3    | Valentia     |   4   |
| Armadura   |  3   | Vitalidad    |   **5**   |
| Suerte     |  6    | Experiencia  |  2    |
| Destreza   |  0    | Percepción   |  0    |

- *Humano-Monje Guerrero*
- **Armas**: Espada ancha, espada falcata, hacha.
- **Armaduras**: Coraza.
- **Equipo**:  
- **Habilidades** (2/8): *[Hechicero Guerrero](https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/DUN/RazasyProfesiones/hechiceroguerrero.jpg), Odio al Submundo, Aguante, Perdido en la Oscuridad*, [Supervivencia](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Habilidades/supervivencia.png?raw=true), Combatir con dos armas.
- **Xp en Pozo de Lucha**: 0/3 (1.Año 1/3)
- **Partidas jugadas:** 8/16
- **Hechizos**: 
-[Saber de la Luz (2/6)](https://github.com/IzaroBlog/IzaroBlog.github.io/tree/main/_images/DUN/Hechizos/Saber%20de%20la%20Luz) :   
	💫 [Arma centelleante](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Saber%20de%20la%20Luz/armacentelleante.png?raw=true)  
	💫 [Luz sanadora](https://github.com/IzaroBlog/IzaroBlog.github.io/blob/main/_images/DUN/Hechizos/Saber%20de%20la%20Luz/luzsanadora.png?raw=true)    

----

| Gastón    |      |      **PV: 12**        |      |
| ---------- | :--: | ------------ | :--: |
| Movimiento |  5    | Agilidad     |   3   |
| Ataque     |  5    | Inteligencia |   4   |
| Fuerza     |  **5**    | Mana         |   0   |
| Disparo    |  3    | Valentia     |   **5**   |
| Armadura   |  3   | Vitalidad    |   5   |
| Suerte     |  6    | Experiencia  |  1    |
| Destreza   |  -1    | Percepción   |  0    |

- *Humano-Caballero*
- **Armas**: Espada ancha
- **Armaduras**: Armadura Completa y Gran Escudo
- **Equipo**:  
- **Habilidades** (0/8): *Voluntad Férrea, Matagigantes, Forzudo, Perdido en la Oscuridad*, una de combate
- **Xp en Pozo de Lucha**: 0/3 (1.Año 1/3)
- **Partidas jugadas:** 3/16

---  

## Partidas jugadas
- Liberad a los mercaderes
- [La Guarida de Rorg](#Rorg-id)
- ¡Lobos!
- Los muertos se levantan
- Reyes bajo la tierra
- M34 Guarida de Bandidos
- Asalto de bandidos
- Defensa de la ciudad
- [La sala de los espejos](#Espejos-id)
- [M39 Trolls Robaovejas](#Trolls-id)
- *Rescatar a Daniel (primer intento)*
- Foso de Lucha
- *Rescatar a Daniel (segundo intento)*
- *Escoltar a Plump (primer intento)*
- *Escoltar a Plump (segundo intento)*
- Información en Thorzul

----

## Intxaur Waldosson {#Intxaur-id}

Entré a la taberna que me había mencionado la posadera. Según parecía, andaban buscando aventureros para formar una banda, y estaban bastante desesperados. La lavandera me dijo que había una bruja detrás de todo aquello, la habían visto hablar con los árboles y repartir pócimas en el río... pero el enlace debía de ser un félido charlatán. Con esas informaciones avancé con paso firme por las calles de Verneck, enfrentándome a las miradas curiosas de los lugareños. ¿Es que nunca habían visto a una enana con un hacha en su vida? 

Con paso ligero, de enana, entré en la taberna del Celíaco Feliz; era fácil reconocerla por su gran hogaza en la puerta. Las risas y buen ambiente que había se iba silenciando mientras avanzaba a la mugrienta barra. ¿Era tan dificil mantener limpia, aunque solo fuese la barra? Estos humanos no tenían ningún sentido de la limpieza... en las tierras enanas por menos que aquello habrían apaleado al tabernero.

Mientras estaba recordando la calidez de las tabernas enanas, sentí un leve golpe en el tobillo y vi mi frente estampada en el inmundo suelo lleno de gluten. Las carcajadas no tardaron en llenar el habitáculo y como siempre, un tipo más grande de lo habitual se levantó y comenzó a gritar: 

- Viktor, desde cuándo permites entrar a muj....

No terminó la frase porque ya tenía un jarra de cerveza estampada en la cara, y para cuándo se dió cuenta un codazo le rompió la nariz. Como siempre, su ayudante de bravuconadas intentó ayudar al señorito, pero recibió un golpe en la entrepierna con el asta del hacha. No merecia la pena desenvainarla para tamaños piltrafas. Un golpe por la espalda que no esperaba la hizo trastabillar, pero hacía falta más fuerza para tumbar a Intxaur Waldosson. Con un ágil movimiento se dió la vuelta y embistió al tercero que quedó tumbado y sin aliento empotrado en la barra. 

Ahora si que el silencio era profundo en la taberna. Sentía el sabor metálico de la sangre en mi boca, y miré en redondo a la concurrencia. Como siempre, la manada con su lider inconsciente en el suelo, no se atrevía a levantar la mirada de sus jarras. Con la mano en el pomo de la espada, avancé directa a por el único que me sostenía la mirada. Tenía que ser este el félido que andaba buscando. 
¿Por qué los hombres hacían siempre las cosas tán difíciles?

# Objetos Mágicos en torres de hechiceria {#objetosmagicos-id}

🧙‍♀️ **THAMEY**:   
- ?
- ?
- ?
🧙‍♀️ **MENON**:
- ?
- ?
- ?

🧙‍♀️ **BIBAL**:
- ?
- ?
- ?	

# Crónicas

## Guarida de bandidos {#Rorg-id}
Intxaur estaba exhausta, su espada bastarda caída en el suelo entre las costillas de un desdichado forajido enano. Escupió sangre y notó que tenía un diente roto al pasar la lengua por su boca. 
Recuperó su mellada espada a dos manos de un tirón y desde el visor de su casco vió el resto de la banda. Eran un par de ballesteros, que sin escapatoria y tras la matanza que habían visto, seguían dispuestos a disparar más virotes. 
Echó de menos su viejo escudo y miró hacia atrás para ver si tenía el apoyo de sus amigas... 
Varyana se había enfrentado al lider de la banda con su daga y estaba exhausta, cubierta de sangre y con un brazo en cabestrillo, no obstante veía la ira en sus ojos, y la mano sana comenzaba a crepitar de magia oscura. Chester no estaba en mejor situación, los hachazos de los forajidos le habían agrietado su armadura de cuero por infinidad de lugares, y tenía una brecha en la ceja, que se empeñaba en vendar con el cinturón de su carcaj. 
Bien, no estoy sola- Pensó para sí. - Se tapó el visor del casco, empuñó la espada y se dirigió con paso firme hacia los ballesteros, mientras escuchaba los versos arcanos de la bruja y el silbido de las flechas de Chester a su lado... Hoy no iban a morir.


## Sala de los espejos {#Espejos-id}
Varyana invocó a sus antiguos dioses élficos y sintió el poder de la lanza en su mano. Krogal estaba despistado controlando a Intxaur y Bergoglio, que se movían como pollos sin cabeza por la sala de los espejos. 

Esta vez el poder de la magia chisporroteó en sus manos y supo al instante que la lanza acabaría en su objetivo. Krogal intentó con todas sus fuerzas dispersar el hechizo pero era demasiado tarde.... la lanza atravesó su oscuro corazón y lo dejó inerte en el suelo, mientras la sangre salpicaba los espejos, reflejando un rojo intenso por toda la sala. 

Chester miró a Varyana confundido, mientras recargaba su arco, todavía no estaba seguro de que el arcano hechicero hubiese muerto. Intxaur y Bergoglio seguían frente a frente, con el gran escudo de Bergoglio roto en el suelo de un hachazo. Se miraron como si se hubiesen acabado de despertar, desafiantes y sin aflojar la tensión en sus armas. 

Varyana, con una voz profunda y cálida les dijo:

- Tranquilos todos, hemos acabado con Krogal pero... es mejor que descanséis fuera de la sala... la magia sigue activa. 

Aturdidos, los tres compañeros salieron de la sala de los espejos, mientras Varyana entraba con paso firme, directa hacia Krogal. El anillo que tenía en su mano, parecía que le estaba llamando por su nombre, y no quería compartir con nadie más su existencia. Se lo arrancó rápidamente de sus huesudos dedos, y el anillo encajó perfectamente en la mano de la bruja. Varyana sentía su poder, y se miró la mano con cautela.... "mi tesoro"

Fuera de la sala, Chester estaba vendando a Bergoglio mientras que Intxaur intentaba arreglar el gran escudo con su cuerda. El félido miró de reojo a Varyana. La elfa estaba observando su mano mientras un extraño fuego salía de las muñecas de la bruja, Chester siguió vendando el brazo del sacerdote mientras pensaba para sí que la magía era demasiado peligrosa....



## Trolls Robaovejas {#Trolls-id}
🤮Puaj qué puto asco!- se quejaba Intxaur mientras intentaba limpiar su armadura de vómito de troll- esto tiene pinta de que no se va a quitar ni con la runa limpiadora de Kragg el Gruñon!! 

✝️Mientras tanto Bergoglio se desesperaba intentando bendecir a Chester; se le había enrollado el rosario entre el arcabuz del félido y el codal de su armadura y no era capaz de restaurar las heridas de esa forma. 
Espera querido hermano- decía con la voz más paciente que pudo encontrar- necesito algo de tiempo para encontrar la señal de mi señor....

🔥 Varyana miró a su alrededor, cadáveres medio calcinados de trolls inundaban la estancia y el olor a carne quemada era insoportable. La senda del fuego había sido muy útil en la mazmorra pero ya tenía ganas de salir de aquel infierno. 
Habrá que ir saliendo de este tugurio!- espetó la bruja- vamos a cobrar una buena recompensa por estos trolls robaovejas en Bibal! Rápido Intxaur corta la cabeza del más grande para llevárnosolo como prueba. 

🪓 Intxaur miró con desprecio al último troll abatido y levantó el hacha -espero que esté bien muerto porque como se empiece a regenerar por el camino, pensó para sí....
